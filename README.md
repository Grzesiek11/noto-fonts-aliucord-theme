# Noto Fonts Aliucord Theme

A simple theme for Aliucord, which replaces the default fonts with Noto Sans
variants

![Theme showcase](doc_assets/showcase.png)

The theme also takes some liberty to use a regular font in some places the
client uses a bold one (`whitney_semibold`), as the author thinks it's
unnecessary in those places.

Requires enabling the **Enable Custom Fonts** setting in Themer plugin options
to work.

## Download

[Theme JSON](https://gitlab.com/Grzesiek11/noto-fonts-aliucord-theme/-/raw/master/Noto_Fonts.json?inline=false)
